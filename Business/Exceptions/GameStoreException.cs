﻿using System.Runtime.Serialization;

namespace Business.Validation
{
    [Serializable]
    public class GameStoreException : Exception
    {
        public GameStoreException()
        {
        }
        
        public GameStoreException(string message) : base(message)
        {
        }

        public GameStoreException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected GameStoreException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}