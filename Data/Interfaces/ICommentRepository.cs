﻿using Data.Entities;

namespace Data.Interfaces
{
    public interface ICommentRepository : IRepository<Comment>
    {
        Task<Comment> GetCommentAsync(int id, int gameId, int userId);
        Task<IEnumerable<Comment>> GetByGameIdAsync(int gameId);
        Task<Comment> GetSpecificCommentAsync(DateTime time);
        Task<SubComment> GetSubCommentAsync(DateTime time);
        Task AddSubCommentAsync(SubComment comment);

    }
}
