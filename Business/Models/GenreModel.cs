﻿using System.ComponentModel.DataAnnotations;

namespace Business.Models
{
    public class GenreModel
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public ICollection<int> GamesIds { get; set; }
        
    }
}
