﻿using Microsoft.EntityFrameworkCore;
using API.Data;
using Data.Entities;
using Data.Interfaces;

namespace Data.Repositories
{
    public class UserRepository : IUsersRepository
    {
        private readonly GameStoreDataContext _context;

        public UserRepository(GameStoreDataContext context)
        {
            _context = context;
        }

        public async Task AddAsync(User entity)
        {
            await _context.Users.AddAsync(entity);
        }

        public void Delete(User entity)
        {
            _context.Users.Remove(entity);
        }

        public async Task<IEnumerable<User>> GetAllAsync()
        {
            return await _context.Users.Include(x=>x.Photos).ToListAsync();
        }

        public async Task<User> GetByEmailAsync(string email)
        {
            return await _context.Users.SingleOrDefaultAsync(x => x.Email == email);
        }

        public async Task<User> GetByIdAsync(int id)
        {
            return await _context.Users.Include(x=>x.Photos).SingleOrDefaultAsync(x => x.Id == id);
        }
        public async Task<IEnumerable<User>> GetByUsernameAsync(string username)
        {
            var users = (await _context.Users.Include(x=>x.Photos).Where(x => x.Username == username).ToListAsync());
            return users.Count==0 ? null : users;
        }

        public void Update(User entity)
        {
            _context.Entry(entity).State = EntityState.Modified;
        }
    }
}
