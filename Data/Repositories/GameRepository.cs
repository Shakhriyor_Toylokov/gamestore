﻿using Microsoft.EntityFrameworkCore;
using API.Data;
using Data.Entities;
using Data.Interfaces;
using Data.Data;

namespace Data.Repositories
{
    public class GameRepository : IGamesRepository
    {
        private readonly GameStoreDataContext _context;

        public GameRepository(GameStoreDataContext context)
        {
            _context = context;
        }
        public async Task AddAsync(Game entity)
        {
            await _context.Games.AddAsync(entity);
        }
        public void Delete(Game entity)
        {
            _context.Games.Remove(entity);
        }

        public async Task<IEnumerable<Game>> GetAllAsync()
        {
            return await _context.Games.Include(x => x.Photos).Include(x => x.Genres).Include(x=>x.Comments).ThenInclude(y => y.SubComments).AsSplitQuery().ToListAsync();
        }

        public async Task<Game> GetByIdAsync(int id)
        {
            return await _context.Games.Include(x => x.Genres).Include(x=>x.Comments).ThenInclude(y=>y.SubComments).Include(x => x.Photos).AsSplitQuery().SingleOrDefaultAsync(x => x.Id == id);
        }

        public async Task<Game> GetByNameAsync(string gameName)
        {
            return await _context.Games.Include(x => x.Genres).Include(x => x.Photos).Include(x=>x.Comments)
                        .ThenInclude(y => y.SubComments).AsSplitQuery()
                        .FirstOrDefaultAsync(x => x.Name.Replace(" ", "").Equals(gameName.Replace(" ", "")));
        }

        public void Update(Game entity)
        {
            _context.Entry(entity).State = EntityState.Modified;
        }
    }
}
