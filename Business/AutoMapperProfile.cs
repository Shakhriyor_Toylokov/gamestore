﻿using AutoMapper;
using Business.DTOs;
using Business.Models;
using Data.Entities;

namespace Business
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<Game, GameModel>()
                .ForMember(g => g.GenreIds, gm => gm.MapFrom(x => x.Genres.Select(y => y.GenreId)));
            CreateMap<GameModel, Game>()
                 .ForMember(g => g.Genres, gm => gm.MapFrom(x => x.GenreIds.Select(y => new GameGenre() { GenreId = y }).ToList()));
            CreateMap<GameGenre, GameGenreModel>()
                .ForMember(ggm => ggm.GenreId, gg => gg.MapFrom(x => x.Genre.Id))
                .ForMember(ggm => ggm.GenreName, gg => gg.MapFrom(x => x.Genre.Name))
                .ReverseMap();
            CreateMap<Photo, PhotoModel>().ReverseMap();
            CreateMap<UserPhoto, PhotoModel>().ReverseMap();
            CreateMap<GamePhoto, PhotoModel>().ReverseMap();

            CreateMap<Genre, GenreModel>()
                .ForMember(gm => gm.GamesIds, g => g.MapFrom(x => x.Games.Select(y => y.GameId)))
                .ReverseMap();
            CreateMap<UserModel, User>().ReverseMap();
            CreateMap<UserModel, UserDto>()
                .ForMember(dest=>dest.PhotoUrl, src=>src.MapFrom(x=>x.Photos.FirstOrDefault(y=>y.IsMain).Url))
                .ReverseMap();
            CreateMap<User, UserDto>()
                .ForMember(dest=>dest.PhotoUrl, src=>src.MapFrom(x=>x.Photos.FirstOrDefault(y=>y.IsMain).Url))
                .ReverseMap();
            CreateMap<RegisterDto, UserModel>();
            CreateMap<RegisterDto, User>().ReverseMap();
            CreateMap<Comment, CommentModel>()
                .ReverseMap();
            CreateMap<SubComment, SubCommentModel>()
                .ReverseMap();
            CreateMap<Order, OrderModel>()
                .ReverseMap();
            CreateMap<OrderDetails, OrderDetailsModel>().ReverseMap();
            
        }
    }
}
