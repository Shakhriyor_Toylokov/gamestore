﻿using Business.Models;

namespace Business.DTOs
{
    public class UserDto
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public string PhotoUrl { get; set; }
        public ICollection<PhotoModel> Photos { get; set; }
        public string Token { get; set; }
    }
}
