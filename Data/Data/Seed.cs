﻿using Microsoft.EntityFrameworkCore;
using System.Text.Json;
using API.Data;
using Data.Entities;

namespace Data.Data
{
    public class Seed
    {
        private const string BaseSeedPath = "bin/Debug/net6.0/Data/SeedData/";
        public static async Task SeedAll(GameStoreDataContext context)
        {
            if (!await context.Games.AnyAsync())
                await SeedGames(context);

            if (!await context.Genres.AnyAsync())
                await SeedGenres(context);

            if (!await context.GameGenres.AnyAsync())
                await SeedGameGenres(context);

            //if (!await context.Comments.AnyAsync())
            //    await SeedComments(context);

            await context.SaveChangesAsync();
        }

        private static async Task SeedGames(GameStoreDataContext context)
        {

            var gameData = await File.ReadAllTextAsync(BaseSeedPath + "GameSeedData.json");
            var games = JsonSerializer.Deserialize<List<Game>>(gameData);
            await context.Games.AddRangeAsync(games);
            await context.SaveChangesAsync();
        }
        private static async Task SeedGenres(GameStoreDataContext context)
        {
            var genreData = await File.ReadAllTextAsync(BaseSeedPath + "GenreSeedData.json");
            var genres = JsonSerializer.Deserialize<List<Genre>>(genreData);
            await context.Genres.AddRangeAsync(genres);
            await context.SaveChangesAsync();
        }

        private static async Task SeedGameGenres(GameStoreDataContext context)
        {
            var gameData = await File.ReadAllTextAsync(BaseSeedPath + "GameGenreSeedData.json");
            var gameGenres = JsonSerializer.Deserialize<List<GameGenre>>(gameData);
            await context.GameGenres.AddRangeAsync(gameGenres);
            await context.SaveChangesAsync();
        }

        private static async Task SeedComments(GameStoreDataContext context)
        {
            var commentData = await File.ReadAllTextAsync(BaseSeedPath + "CommentSeedData.json");
            var comments = JsonSerializer.Deserialize<List<Comment>>(commentData);
       //     await context.Comments.AddRangeAsync(comments);
            await context.SaveChangesAsync();
        }
    }
}
