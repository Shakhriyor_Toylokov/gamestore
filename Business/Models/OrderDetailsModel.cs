﻿using Data.Entities;

namespace Business.Models
{
    public class OrderDetailsModel
    {
        public int Id { get; set; }
        public int GameId { get; set; }
        public int Quantity { get; set; }
        public decimal TotalPrice { get; set; }
    }
}

