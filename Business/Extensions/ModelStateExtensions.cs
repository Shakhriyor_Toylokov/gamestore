﻿using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace Business.Extensions
{
    public static class ModelStateExtensions
    {
        public static IEnumerable<ModelError> ErrorMessages(this ModelStateDictionary dictionary)
        {
            return dictionary.Values.SelectMany(x => x.Errors);
        }
    }
}
