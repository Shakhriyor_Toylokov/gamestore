﻿using Business.Helpers;
using Business.Models;
using Microsoft.AspNetCore.Http;

namespace Business.Interfaces
{
    public interface IGameService : ICrud<GameModel>
    {
        Task<GameModel> GetByNameAsync(string gameName);
        Task<IEnumerable<GameModel>> GetByParamsAsync(GameParams gameParams);
        Task<PhotoModel> AddPhotoAsync(IFormFile file, int id);
        Task DeletePhotoAsync(int gameId,int photoId);
        Task SetMainPhotoAsync(int gameId, int photoId);

    }
}
