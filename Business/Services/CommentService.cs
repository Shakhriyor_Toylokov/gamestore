﻿using AutoMapper;
using Business.Interfaces;
using Business.Models;
using Data.Entities;
using Data.Interfaces;

namespace Business.Services
{
    public class CommentService : ICommentService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public CommentService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task AddAsync(CommentModel model)
        {
            await _unitOfWork.CommentRepository.AddAsync(_mapper.Map<Comment>(model));
            await _unitOfWork.SaveAsync();

        }

        public async Task<CommentModel> AddCommentAsync(CommentModel model)
        {
            await AddAsync(model);
            var addedComment = await _unitOfWork.CommentRepository.GetSpecificCommentAsync(model.Time);
            return _mapper.Map<CommentModel>(addedComment);
        }

        public async Task DeleteAsync(int modelId)
        {
            var comment = await _unitOfWork.CommentRepository.GetByIdAsync(modelId) ??
                          throw new KeyNotFoundException("Comment is not found");
            _unitOfWork.CommentRepository.Delete(comment);
            await _unitOfWork.SaveAsync();
        }

        public async Task<IEnumerable<CommentModel>> GetAllAsync()
        {
            return _mapper.Map<IEnumerable<CommentModel>>(await _unitOfWork.CommentRepository.GetAllAsync());
        }

        public async Task<CommentModel> GetByIdAsync(int id)
        {
            var comment = await _unitOfWork.CommentRepository.GetByIdAsync(id) ??
                          throw new KeyNotFoundException("Comment is not found");
            return _mapper.Map<CommentModel>(comment);
        }
        public async Task<IEnumerable<CommentModel>> GetByGameIdAsync(int gameId)
        {
            _ = await _unitOfWork.GamesRepository.GetByIdAsync(gameId) ??
                       throw new KeyNotFoundException("Game is not found");
            return _mapper.Map<IEnumerable<CommentModel>>(await _unitOfWork.CommentRepository.GetByGameIdAsync(gameId));
        }

        public async Task UpdateAsync(CommentModel model)
        {
            if(await GetByIdAsync(model.Id) != null)
            {
                _unitOfWork.CommentRepository.Update(_mapper.Map<Comment>(model));
                await _unitOfWork.SaveAsync();
            }
        }

        public async Task<SubCommentModel> AddSubCommentAsync(SubCommentModel model)
        {
            await _unitOfWork.CommentRepository.AddSubCommentAsync(_mapper.Map<SubComment>(model));
            await _unitOfWork.SaveAsync();
            return _mapper.Map<SubCommentModel>(await _unitOfWork.CommentRepository.GetSubCommentAsync(model.Time));
        }
    }
}
