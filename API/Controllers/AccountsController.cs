﻿using Microsoft.AspNetCore.Mvc;
using Business.Interfaces;
using Business.Models;
using Business.DTOs;
using Business.Extensions;

namespace API.Controllers
{
    public class AccountsController : BaseApiController
    {
        private readonly IUserService _userService;

        public AccountsController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpPost("register")]
        public async Task<ActionResult<UserDto>> Register(RegisterDto registerDto)
        {
            var user = await _userService.Register(registerDto);
            return Created("GetUser", user);
        }

        [HttpPost("login")]
        public async Task<ActionResult<UserDto>> Login(LoginDto loginDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState.ErrorMessages());
            }
            var user = await _userService.Login(loginDto);
            return Ok(user);
        }
    }
}
