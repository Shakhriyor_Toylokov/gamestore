﻿namespace Business.Models
{
    public class OrderModel
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string PaymentType { get; set; }
        public string OrderComment { get; set; }
        public decimal TotalPrice { get; set; }
        public ICollection<OrderDetailsModel> Details { get; set; }
    }
}
