﻿using Microsoft.AspNetCore.Http;
using Business.Interfaces;
using Business.Models;
using Business.Validation;
using Data.Entities;
using Business.Helpers;
using Data.Interfaces;
using AutoMapper;
using Data.Repositories;

namespace Business.Services
{
    public class GameService : IGameService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IPhotoService _photoService;

        public GameService(IUnitOfWork unitOfWork, IMapper mapper, IPhotoService photoService)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _photoService = photoService;
        }
        public async Task AddAsync(GameModel model)
        {
            if (await _unitOfWork.GamesRepository.GetByNameAsync(model.Name) != null)
            {
                throw new GameStoreException("Game with the given name already exists. Try another name");
            }
            await _unitOfWork.GamesRepository.AddAsync(_mapper.Map<Game>(model));
            await _unitOfWork.SaveAsync();
        }

        public async Task<PhotoModel> AddPhotoAsync(IFormFile file, int id)
        {
            var result = await _photoService.AddPhotoAsync(file);
            if (result.Error != null)
            {
                throw new PhotoException("Photo could not be uploaded");
            }

            var photo = new GamePhoto
            {
                Url = result.SecureUrl.AbsoluteUri,
                PublicId = result.PublicId
            };
            var game = await _unitOfWork.GamesRepository.GetByIdAsync(id);
            if (game.Photos.Count == 0)
            {
                photo.IsMain = true;
            }
            game.Photos.Add(photo);

            await _unitOfWork.SaveAsync();

            return _mapper.Map<PhotoModel>(photo);
        }
        public async Task SetMainPhotoAsync(int gameId, int photoId)
        {
            var game = await _unitOfWork.GamesRepository.GetByIdAsync(gameId);
            var photo = game.Photos.FirstOrDefault(x => x.Id == photoId);
            if (photo == null)
                throw new KeyNotFoundException("Photo does not exist!");
            if (photo.IsMain)
                throw new PhotoException("The photo is already a main photo");
            var currentMain = game.Photos.FirstOrDefault(x => x.IsMain);
            if (currentMain != null)
                currentMain.IsMain = false;
            photo.IsMain = true;
            await _unitOfWork.SaveAsync();
        }
        public async Task DeletePhotoAsync(int gameId, int photoId)
        {
            var game = await _unitOfWork.GamesRepository.GetByIdAsync(gameId);
            var photo = game.Photos.FirstOrDefault(x => x.Id == photoId);
            if (photo == null)
                throw new KeyNotFoundException("Photo does not exist!");
            if (photo.PublicId != null)
            {
                var result = await _photoService.DeletePhotoAsync(photo.PublicId);
                if (result.Error != null)
                    throw new PhotoException("Error occured while deleting a photo");

            }

            game.Photos.Remove(photo);

            await _unitOfWork.SaveAsync();
        }
        public async Task DeleteAsync(int modelId)
        {
            var game = await _unitOfWork.GamesRepository.GetByIdAsync(modelId)
                       ?? throw new KeyNotFoundException("Game is not found");
            _unitOfWork.GamesRepository.Delete(game);
            await _unitOfWork.SaveAsync();

        }

        public async Task<IEnumerable<GameModel>> GetAllAsync()
        {
            var games = await _unitOfWork.GamesRepository.GetAllAsync();
            return _mapper.Map<IEnumerable<GameModel>>(games);
        }

        public async Task<IEnumerable<GameModel>> GetByParamsAsync(GameParams gameParams)
        {
            if (await _unitOfWork.GenreRepository.GetByIdAsync(gameParams.GenreId) == null)
                throw new KeyNotFoundException("Genre is not found!");
            var games = await _unitOfWork.GamesRepository.GetAllAsync();
            var result = games.Where(x => x.Genres.Any(x => x.GenreId == gameParams.GenreId));
            return _mapper.Map<IEnumerable<GameModel>>(result);
        }

        public async Task<GameModel> GetByIdAsync(int id)
        {
            var game = await _unitOfWork.GamesRepository.GetByIdAsync(id)
                    ?? throw new KeyNotFoundException("Game is not found!");

            return _mapper.Map<GameModel>(game);
        }

        public async Task<GameModel> GetByNameAsync(string gameName)
        {
            var game = await _unitOfWork.GamesRepository.GetByNameAsync(gameName);
            if (game == null)
                throw new KeyNotFoundException("Game is not found!");

            return _mapper.Map<GameModel>(game);
        }
        public async Task UpdateAsync(GameModel model)
        {
            
            _unitOfWork.GamesRepository.Update(_mapper.Map<Game>(model));
            await _unitOfWork.SaveAsync();
        }

    }
}
