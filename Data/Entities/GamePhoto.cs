﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Entities
{
    public class GamePhoto : Photo
    {
        public Game Game { get; set; }
        public int GameId { get; set; }
    }
}
