﻿using Microsoft.EntityFrameworkCore;
using API.Data;
using Data.Entities;
using Data.Interfaces;

namespace Data.Repositories
{
    public class GenreRepository : IGenreRepository
    {
        private readonly GameStoreDataContext _context;

        public GenreRepository(GameStoreDataContext context)
        {
            _context = context;
        }
        public async Task AddAsync(Genre entity)
        {
            await _context.Genres.AddAsync(entity);
        }

        public void Delete(Genre entity)
        {
            _context.Genres.Remove(entity);
        }

        public async Task<IEnumerable<Genre>> GetAllAsync()
        {
            return await _context.Genres.ToListAsync();
        }

        public async Task<Genre> GetByIdAsync(int id)
        {
            return await _context.Genres.SingleOrDefaultAsync(x => x.Id == id);
        }

        public void Update(Genre entity)
        {
            _context.Entry(entity).State = EntityState.Modified;
        }
    }
}
