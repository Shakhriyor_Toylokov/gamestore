﻿using AutoMapper;
using Business.Interfaces;
using Business.Models;
using Data.Entities;
using Data.Interfaces;

namespace Business.Services
{
    public class GenreService : IGenreService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public GenreService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        public async Task AddAsync(GenreModel model)
        {
            await _unitOfWork.GenreRepository.AddAsync(_mapper.Map<Genre>(model));
            await _unitOfWork.SaveAsync();
        }

        public async Task DeleteAsync(int modelId)
        {
            var genre = await _unitOfWork.GenreRepository.GetByIdAsync(modelId)
                        ?? throw new KeyNotFoundException("Genre is not found");

            _unitOfWork.GenreRepository.Delete(genre);
            await _unitOfWork.SaveAsync();
        }

        public async Task<IEnumerable<GenreModel>> GetAllAsync()
        {
            return _mapper.Map<IEnumerable<GenreModel>>(await _unitOfWork.GenreRepository.GetAllAsync());
        }

        public async Task<GenreModel> GetByIdAsync(int id)
        {
            var genre = await _unitOfWork.GenreRepository.GetByIdAsync(id)
                        ?? throw new KeyNotFoundException("Genre is not found!");
            return _mapper.Map<GenreModel>(genre);
        }

        public async Task UpdateAsync(GenreModel model)
        {
            _unitOfWork.GenreRepository.Update(_mapper.Map<Genre>(model));
            await _unitOfWork.SaveAsync();
        }

    }
}
