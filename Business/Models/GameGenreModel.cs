﻿namespace Business.Models
{
    public class GameGenreModel
    { 
        public int GenreId { get; set; }
        public string GenreName { get; set; }
    }
}