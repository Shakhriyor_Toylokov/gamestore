﻿using Data.Entities;
using System.ComponentModel.DataAnnotations;

namespace Business.Models
{
    public class CommentModel : BaseCommentModel
    {
        public int GameId { get; set; }
        public int UserId  { get; set; }
        public ICollection<SubCommentModel> SubComments { get; set; }

    }
}
