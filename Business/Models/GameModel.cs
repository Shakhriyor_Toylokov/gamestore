﻿using System.ComponentModel.DataAnnotations;

namespace Business.Models
{
    public class GameModel
    {
        public int Id { get; set; }
        [Required]
        [StringLength(60, MinimumLength = 2)]
        public string Name { get; set; }
        public decimal? Price { get; set; } = 0;
        [Required]
        [StringLength(1000,MinimumLength = 10)]
        public string Description { get; set; }
        public ICollection<int> GenreIds { get; set; } = new List<int>() { };
        public ICollection<PhotoModel> Photos { get; set; }
        public ICollection<CommentModel> Comments { get; set; }


    }
}
