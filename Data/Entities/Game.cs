﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
namespace Data.Entities
{
    public class Game : BaseEntity
    {
        public string Name { get; set; }
        public decimal Price { get; set; }
        public string Description { get; set; }
        public ICollection<GameGenre> Genres { get; set; }
        public ICollection<GamePhoto> Photos { get; set; }
        public ICollection<Comment> Comments { get; set; }

    }
}
