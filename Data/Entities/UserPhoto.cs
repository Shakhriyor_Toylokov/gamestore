﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Entities
{
    public class UserPhoto : Photo
    {
        public User User { get; set; }
        public int UserId { get; set; }

    }
}
