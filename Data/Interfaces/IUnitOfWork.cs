﻿namespace Data.Interfaces
{
    public interface IUnitOfWork
    {
        IGamesRepository GamesRepository { get; }
        IGenreRepository GenreRepository { get; }
        IUsersRepository UsersRepository { get; }   
        ICommentRepository CommentRepository { get; }
        IOrderRepository OrderRepository { get; }
        Task SaveAsync();
    }
}
