﻿using Business.Models;

namespace Business.Interfaces
{
    public interface ICommentService: ICrud<CommentModel>
    {
        Task<IEnumerable<CommentModel>> GetByGameIdAsync(int gameId);
        Task<CommentModel> AddCommentAsync(CommentModel model);
        Task<SubCommentModel> AddSubCommentAsync(SubCommentModel model);

    }
}
