﻿using Business.Extensions;
using Business.Interfaces;
using Business.Models;
using Business.Services;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    public class OrdersController : BaseApiController
    {
        private readonly IOrderService _orderService;

        public OrdersController(IOrderService orderService)
        {
            _orderService = orderService;
        }
        [HttpGet]
        public async Task<ActionResult<IEnumerable<OrderModel>>> Get()
        {
            var orders = await _orderService.GetAllAsync();
            return Ok(orders);
        }

        [HttpGet("{id}", Name = "GetOrder")]
        public async Task<ActionResult<OrderModel>> GetById(int id)
        {
            var order = await _orderService.GetByIdAsync(id);
            return Ok(order);
        }

        [HttpPost]
        public async Task<ActionResult> Add([FromBody] OrderModel model)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState.ErrorMessages());

            await _orderService.AddAsync(model);
            return Created("GetOrder", model);
        }

        [HttpPut]
        public async Task<ActionResult> Update([FromBody] OrderModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState.ErrorMessages());
            }
            await _orderService.UpdateAsync(model);
            return Ok(model);
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            await _orderService.DeleteAsync(id);
            return Ok();
        }
    }
}
