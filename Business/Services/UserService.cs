﻿using Microsoft.AspNetCore.Http;
using System.Security.Cryptography;
using System.Text;
using AutoMapper;
using Business.DTOs;
using Business.Interfaces;
using Business.Models;
using Business.Validation;
using Data.Entities;
using Data.Interfaces;

namespace Business.Services
{
    public class UserService : IUserService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly ITokenService _tokenService;
        private readonly IPhotoService _photoService;

        public UserService(IUnitOfWork unitOfWork, IMapper mapper, ITokenService tokenService, IPhotoService photoService)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _tokenService = tokenService;
            _photoService = photoService;
        }

        private async Task AddAsync(RegisterDto model)
        {
            if (await _unitOfWork.UsersRepository.GetByEmailAsync(model.Email) != null)
            {
                throw new ApplicationException("Email is already taken! Try another email!");
            }
            using var hmac = new HMACSHA512();
            var user = _mapper.Map<User>(model);
            user.PasswordHash = hmac.ComputeHash(Encoding.UTF8.GetBytes(model.Password));
            user.PasswordSalt = hmac.Key;
            await _unitOfWork.UsersRepository.AddAsync(user);
            await _unitOfWork.SaveAsync();
        }

        public async Task DeleteAsync(int modelId)
        {
            var user = await _unitOfWork.UsersRepository.GetByIdAsync(modelId)
                       ?? throw new KeyNotFoundException("User is not found");
            _unitOfWork.UsersRepository.Delete(user);
            await _unitOfWork.SaveAsync();
        }
        public async Task<IEnumerable<UserDto>> GetAllAsync()
        {
            return _mapper.Map<IEnumerable<UserDto>>(await _unitOfWork.UsersRepository.GetAllAsync());
        }

        private async Task<UserModel> GetByIdAsync(int id)
        {
            var user = await _unitOfWork.UsersRepository.GetByIdAsync(id)
                    ?? throw new KeyNotFoundException("User is not found!");

            return _mapper.Map<UserModel>(user);
        }
        public async Task<UserModel> GetByEmailAsync(string email)  
        {
            var user = await _unitOfWork.UsersRepository.GetByEmailAsync(email)
                    ?? throw new KeyNotFoundException("User is not found!");
            return _mapper.Map<UserModel>(user);
        }
        public async Task<UserDto> GetSpecificUserWithTokenAsync(int id)
        {
            var user = await _unitOfWork.UsersRepository.GetByIdAsync(id)
                    ?? throw new KeyNotFoundException("User is not found!");
            var userToReturn = _mapper.Map<UserDto>(user);
            userToReturn.Token = _tokenService.CreateToken(_mapper.Map<UserModel>(user));
            return userToReturn;
        }
        
        public async Task UpdateUserDetailsAsync(UserDto model)
        {
            _unitOfWork.UsersRepository.Update(_mapper.Map<User>(model));
            await _unitOfWork.SaveAsync();
        }
        public async Task<UserDto> Register(RegisterDto model)
        {
            await AddAsync(model);
            var userToReturn = _mapper.Map<UserDto>(await GetByEmailAsync(model.Email));
            userToReturn.Token = _tokenService.CreateToken(_mapper.Map<UserModel>(model));
            return userToReturn;
        }
        public async Task<UserDto> Login(LoginDto loginDto)
        {
            var users = await _unitOfWork.UsersRepository.GetByUsernameAsync(loginDto.Username) ??
                        throw new UnauthorizedAccessException("Invalid username");
            foreach (var user in users)
            {
                if (CheckPassword(user, loginDto.Password))
                {
                    var userToReturn = _mapper.Map<UserDto>(user);
                    userToReturn.Token = _tokenService.CreateToken(_mapper.Map<UserModel>(user));
                    return userToReturn;
                }
            }
            throw new UnauthorizedAccessException("Invalid password");
        }


        public async Task<PhotoModel> AddPhotoAsync(IFormFile file, int id)
        {
            var result = await _photoService.AddPhotoAsync(file);
            if (result.Error != null)
            {
                throw new PhotoException("Photo could not be uploaded");
            }

            var photo = new UserPhoto
            {
                Url = result.SecureUrl.AbsoluteUri,
                PublicId = result.PublicId
            };
            var user = await _unitOfWork.UsersRepository.GetByIdAsync(id);
            if (user.Photos.Count == 0)
            {
                photo.IsMain = true;
            }
            user.Photos.Add(photo);

            await _unitOfWork.SaveAsync();

            return _mapper.Map<PhotoModel>(photo);
        }
        public async Task SetMainPhotoAsync(int gameId, int photoId)
        {
            var user = await _unitOfWork.UsersRepository.GetByIdAsync(gameId);
            var photo = user.Photos.FirstOrDefault(x => x.Id == photoId);
            if (photo == null)
                throw new KeyNotFoundException("Photo does not exist!");
            
            if (photo.IsMain)
                throw new PhotoException("The photo is already a main photo");

            var currentMain = user.Photos.FirstOrDefault(x => x.IsMain);
            if (currentMain != null)
                currentMain.IsMain = false;

            photo.IsMain = true;
            await _unitOfWork.SaveAsync();
        }
        public async Task DeletePhotoAsync(int gameId, int photoId)
        {
            var user = await _unitOfWork.UsersRepository.GetByIdAsync(gameId);
            var photo = user.Photos.FirstOrDefault(x => x.Id == photoId);
            if (photo == null)
                throw new KeyNotFoundException("Photo does not exist!");
            if (photo.PublicId != null)
            {
                var result = await _photoService.DeletePhotoAsync(photo.PublicId);
                if (result.Error != null)
                    throw new PhotoException("Error occured while deleting a photo");
            }

            user.Photos.Remove(photo);

            await _unitOfWork.SaveAsync();
        }
        private static bool CheckPassword(User user, string password)
        {
            using var hmac = new HMACSHA512(user.PasswordSalt);

            var computedHash = hmac.ComputeHash(Encoding.UTF8.GetBytes(password));
            for (int i = 0; i < computedHash.Length; i++)
            {
                if (computedHash[i] != user.PasswordHash[i])
                    return false;
            }
            return true;
        }

        public async Task<List<string>> GetEmailsAsync()
        {
            var users= await GetAllAsync();
            var emails= new List<string>();
            foreach (var user in users)
            {
                emails.Add(user.Email);
            }
            return emails;
        }

       
    }
}
