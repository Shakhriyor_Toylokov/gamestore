﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Models
{
    public class BaseCommentModel
    {
        public int Id { get; set; }
        [Required]
        [StringLength(600, MinimumLength = 1)]
        public string Text { get; set; }
        public DateTime Time { get; set; } = DateTime.Now;
    }
}
