﻿using Business.Models;
using System.ComponentModel.DataAnnotations;

namespace Business.DTOs
{
    public class RegisterDto
    {
        public int Id { get; set; }
        [StringLength(40, MinimumLength = 2)]
        public string FirstName { get; set; }
        [StringLength(40, MinimumLength = 2)]
        public string LastName { get; set; }
        [StringLength(40, MinimumLength = 3)]
        [Required]
        public string Username { get; set; }
        [StringLength(60, MinimumLength = 4)]
        [Required]
        public string Email { get; set; }
        [Required]
        public string Password { get; set; }
    }
}
