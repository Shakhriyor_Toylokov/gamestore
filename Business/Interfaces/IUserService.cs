﻿using Business.DTOs;
using Business.Models;
using Microsoft.AspNetCore.Http;

namespace Business.Interfaces
{
    public interface IUserService
    {
        Task<UserDto> Register(RegisterDto model);
        Task<UserDto> Login(LoginDto loginDto);
        Task<PhotoModel> AddPhotoAsync(IFormFile file, int id);
        Task DeletePhotoAsync(int userId, int photoId);
        Task SetMainPhotoAsync(int userId, int photoId);
        Task<IEnumerable<UserDto>> GetAllAsync();
        Task<UserDto> GetSpecificUserWithTokenAsync(int id);
        Task<List<string>> GetEmailsAsync();
        Task UpdateUserDetailsAsync(UserDto model);
        Task DeleteAsync(int modelId);
    }
}
