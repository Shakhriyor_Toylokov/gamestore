﻿using Microsoft.AspNetCore.Mvc;
using Business.Extensions;
using Business.Interfaces;
using Business.Models;
using Business.Validation;
using Business.DTOs;
using Microsoft.AspNetCore.Authorization;

namespace API.Controllers
{
    [Authorize]
    public class UsersController : BaseApiController
    {
        private readonly IUserService _userService;

        public UsersController(IUserService userService)
        {
            _userService = userService;
        }

        [AllowAnonymous]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<UserDto>>> Get()
        {
            var users = await _userService.GetAllAsync();
            return Ok(users);
        }

        [AllowAnonymous]
        [HttpGet("emails")]
        public async Task<ActionResult<List<string>>> GetEmails()
        {
            return await _userService.GetEmailsAsync(); ;
        }

        [HttpGet("{id}", Name = "GetUser")]
        public async Task<ActionResult<UserDto>> GetById(int id)
        {
            var user = await _userService.GetSpecificUserWithTokenAsync(id);
            return Ok(user);
        }
        [HttpPost("{id}/add-photo")]
        public async Task<ActionResult<PhotoModel>> AddPhoto(int id, IFormFile file)
        {
            var photo = await _userService.AddPhotoAsync(file, id);
            return CreatedAtRoute("GetUser", new { id }, photo);
        }
        [HttpPut("{id}/set-main-photo/{photoId}")]
        public async Task<ActionResult> SetMainPhoto(int id, int photoId)
        {
            await _userService.SetMainPhotoAsync(id, photoId);
            return Ok();
        }
        [HttpDelete("{id}/delete-photo/{photoId}")]
        public async Task<ActionResult> DeletePhoto(int id, int photoId)
        {
            await _userService.DeletePhotoAsync(id, photoId);
            return Ok();
        }
        [HttpPut]
        public async Task<ActionResult> Update([FromBody] UserDto model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState.ErrorMessages());
            }
            await _userService.UpdateUserDetailsAsync(model);
            return Ok(model);
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            await _userService.DeleteAsync(id);
            return Ok();
        }
    }
}
