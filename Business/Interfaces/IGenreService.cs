﻿using Business.Models;

namespace Business.Interfaces
{
    public interface IGenreService : ICrud<GenreModel>
    {
    }
}
