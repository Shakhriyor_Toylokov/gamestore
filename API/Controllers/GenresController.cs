﻿using Microsoft.AspNetCore.Mvc;
using Business.Interfaces;
using Business.Models;
using Business.Extensions;

namespace API.Controllers
{
    public class GenresController : BaseApiController
    {
        private readonly IGenreService _genreService;

        public GenresController(IGenreService genreService)
        {
            _genreService = genreService;
        }
        [HttpGet]
        public async Task<ActionResult<IEnumerable<GenreModel>>> Get()
        {
            var genres = await _genreService.GetAllAsync();
            return Ok(genres);
        }

        [HttpGet("{id}", Name = "GetGenre")]
        public async Task<ActionResult<GenreModel>> GetById(int id)
        {
            GenreModel genre;
                genre = await _genreService.GetByIdAsync(id);
            return Ok(genre);
        }

        [HttpPost]
        public async Task<ActionResult> Add([FromBody] GenreModel model)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState.ErrorMessages());

            await _genreService.AddAsync(model);
            return Created("GetGenre", model);
        }

        [HttpPut]
        public async Task<ActionResult> Update([FromBody] GenreModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState.ErrorMessages());
            }
            await _genreService.UpdateAsync(model);
            return Ok(model);
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            await _genreService.DeleteAsync(id);
            return Ok();
        }
    }
}
