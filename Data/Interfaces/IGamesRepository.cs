﻿using Data.Entities;

namespace Data.Interfaces
{
    public interface IGamesRepository : IRepository<Game>
    {
        Task<Game> GetByNameAsync(string gameName);
    }
}
