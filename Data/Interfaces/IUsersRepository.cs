﻿using Data.Entities;

namespace Data.Interfaces
{
    public interface IUsersRepository : IRepository<User>
    {
        Task<IEnumerable<User>> GetByUsernameAsync(string username);
        Task<User> GetByEmailAsync(string email);
        
    }
}
