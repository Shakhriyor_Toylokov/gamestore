﻿using System.ComponentModel.DataAnnotations;

namespace Business.Models
{
    public class UserModel  
    {
        public int Id { get; set; }
        [StringLength(40, MinimumLength = 2)]
        public string FirstName { get; set; }
        [StringLength(40, MinimumLength = 2)]
        public string LastName { get; set; }
        [StringLength(40, MinimumLength = 3)]
        [Required]
        public string Username { get; set; }
        [StringLength(60, MinimumLength = 4)]
        [Required]
        public string Email { get; set; }
        public ICollection<PhotoModel> Photos { get; set; }
        
    }
}
