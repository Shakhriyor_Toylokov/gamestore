﻿using Data.Entities;
using Microsoft.EntityFrameworkCore;

namespace API.Data
{
    public class GameStoreDataContext : DbContext
    {
        public GameStoreDataContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<Game> Games { get; set; }
        public DbSet<Genre> Genres { get; set; }
        public DbSet<GameGenre> GameGenres { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<SubComment> SubComments { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderDetails> OrderDetails { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Game>().Property(p => p.Price).HasColumnType("decimal(18,4)");
            modelBuilder.Entity<Game>().Property(g => g.Name).IsRequired().HasMaxLength(60);
            modelBuilder.Entity<Game>().HasIndex(g => g.Name).IsUnique();
            modelBuilder.Entity<Game>().Property(g => g.Description).IsRequired().HasMaxLength(1000);
            modelBuilder.Entity<Genre>().Property(g => g.Name).IsRequired().HasMaxLength(40);
            modelBuilder.Entity<GameGenre>().HasKey(g => new { g.GameId, g.GenreId });
            modelBuilder.Entity<User>().Property(u => u.Username).IsRequired().HasMaxLength(60);
            modelBuilder.Entity<User>().Property(u => u.Email).IsRequired().HasMaxLength(40);
            modelBuilder.Entity<User>().HasIndex(u => u.Email).IsUnique();
            modelBuilder.Entity<User>().Property(u => u.FirstName).HasMaxLength(40);
            modelBuilder.Entity<User>().Property(u => u.LastName).HasMaxLength(40);
            modelBuilder.Entity<Comment>().Property(c => c.Text).IsRequired().HasMaxLength(600);
            modelBuilder.Entity<SubComment>().Property(c => c.Text).IsRequired().HasMaxLength(600);
            modelBuilder.Entity<Order>().Property(c => c.FirstName).IsRequired().HasMaxLength(60);
            modelBuilder.Entity<Order>().Property(c => c.LastName).IsRequired().HasMaxLength(60);
            modelBuilder.Entity<Order>().Property(c => c.Email).IsRequired().HasMaxLength(60);
            modelBuilder.Entity<Order>().Property(c => c.PaymentType).IsRequired().HasMaxLength(40);
            modelBuilder.Entity<Order>().Property(c => c.Phone).IsRequired().HasMaxLength(60);
            modelBuilder.Entity<Order>().Property(c => c.OrderComment).HasMaxLength(600);
            


        }
    }
}
