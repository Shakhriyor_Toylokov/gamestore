﻿using AutoMapper;
using Business.Interfaces;
using Business.Models;
using Data.Entities;
using Data.Interfaces;

namespace Business.Services
{
    public class OrderService : IOrderService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public OrderService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task AddAsync(OrderModel model)
        {
            await _unitOfWork.OrderRepository.AddAsync(_mapper.Map<Order>(model));
            await _unitOfWork.SaveAsync();
        }

        public async Task DeleteAsync(int modelId)
        {
            var order = await _unitOfWork.OrderRepository.GetByIdAsync(modelId)
                       ?? throw new KeyNotFoundException("Order is not found");
            _unitOfWork.OrderRepository.Delete(order);
            await _unitOfWork.SaveAsync();
        }

        public async Task<IEnumerable<OrderModel>> GetAllAsync()
        {
            var orders = await _unitOfWork.OrderRepository.GetAllAsync();
            return _mapper.Map<IEnumerable<OrderModel>>(orders);
        }

        public async Task<OrderModel> GetByIdAsync(int id)
        {
            var order = await _unitOfWork.OrderRepository.GetByIdAsync(id)
                    ?? throw new KeyNotFoundException("Order is not found!");

            return _mapper.Map<OrderModel>(order);
        }

        public async Task UpdateAsync(OrderModel model)
        {
            _unitOfWork.OrderRepository.Update(_mapper.Map<Order>(model));
            await _unitOfWork.SaveAsync();
        }
    }
}
