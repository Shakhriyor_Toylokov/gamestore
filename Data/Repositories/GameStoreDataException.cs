﻿using System.Runtime.Serialization;

namespace Data.Repositories
{
    [Serializable]
    internal class GameStoreDataException : Exception
    {
        public GameStoreDataException()
        {
        }

        public GameStoreDataException(string message) : base(message)
        {
        }

        public GameStoreDataException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected GameStoreDataException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}