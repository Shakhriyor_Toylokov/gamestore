﻿using System.Runtime.Serialization;

namespace Business.Validation
{
    [Serializable]
    public class PhotoException : Exception
    {
        public PhotoException()
        {
        }

        public PhotoException(string message) : base(message)
        {
        }

        public PhotoException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected PhotoException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}