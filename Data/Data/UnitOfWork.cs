﻿using API.Data;
using Data.Interfaces;
using Data.Repositories;

namespace Data.Data
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly GameStoreDataContext _context;
        private GameRepository _gameRepository;
        private GenreRepository _genreRepository;
        private UserRepository _userRepository;
        private CommentRepository _commentRepository;
        private OrderRepository _orderRepository;
        public UnitOfWork(GameStoreDataContext context)
        {
            _context = context;
        }

        public IGamesRepository GamesRepository
        {
            get
            {
                _gameRepository ??= new GameRepository(_context);
                return _gameRepository;
            }
        }

        public IGenreRepository GenreRepository
        {
            get
            {
                _genreRepository ??= new GenreRepository(_context);
                return _genreRepository;
            }
        }

        public IUsersRepository UsersRepository
        {
            get
            {
                _userRepository ??= new UserRepository(_context);
                return _userRepository;
            }
        }

        public ICommentRepository CommentRepository
        {
            get
            {
                _commentRepository ??= new CommentRepository(_context);
                return _commentRepository;
            }
        }

        public IOrderRepository OrderRepository
        {
            get
            {
                _orderRepository ??= new OrderRepository(_context);
                return _orderRepository;
            }
        }
        public async Task SaveAsync()
        {
            await _context.SaveChangesAsync();
        }
    }
}
