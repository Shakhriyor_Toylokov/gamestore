﻿using Microsoft.EntityFrameworkCore;
using API.Data;
using Data.Entities;
using Data.Interfaces;

namespace Data.Repositories
{
    public class CommentRepository : ICommentRepository
    {
        private readonly GameStoreDataContext _context;

        public CommentRepository(GameStoreDataContext context)
        {
            _context = context;
        }

        public async Task AddAsync(Comment entity)
        {
            await _context.Comments.AddAsync(entity);
        }
        public async Task AddSubCommentAsync(SubComment entity)
        {
            await _context.SubComments.AddAsync(entity);
        }
        public void Delete(Comment entity)
        {
            _context.Comments.Remove(entity);
        }

        public async Task<IEnumerable<Comment>> GetAllAsync()
        {
            return await _context.Comments.Include(x => x.SubComments).Include(x => x.Game).ThenInclude(y => y.Photos).AsSplitQuery().ToListAsync();
        }
        public async Task<Comment> GetSpecificCommentAsync(DateTime time)
        {
            return await _context.Comments.Include(x => x.SubComments).Include(x => x.Game)
                         .ThenInclude(y =>y.Photos).AsSplitQuery().AsNoTracking().FirstOrDefaultAsync(x => x.Time == time);
        }
        public async Task<IEnumerable<Comment>> GetByGameIdAsync(int gameId)
        {
            return await _context.Comments.Where(x => x.GameId == gameId).Include(x => x.SubComments)
                                          .Include(x => x.Game).ThenInclude(y => y.Photos).AsSplitQuery().ToListAsync();

        }

        public async Task<Comment> GetByIdAsync(int id)
        {
            return await _context.Comments.Include(x => x.SubComments).Include(x => x.Game).ThenInclude(y => y.Photos).AsSplitQuery().AsNoTracking().SingleOrDefaultAsync(x => x.Id == id);
        }
        public async Task<Comment> GetCommentAsync(int id, int gameId, int userId)
        {
            return await _context.Comments.Include(x => x.SubComments).Include(x => x.Game)
                                 .SingleOrDefaultAsync(x => x.Id == id && x.UserId == userId && x.GameId == gameId);
        }
        public void Update(Comment entity)
        {
            _context.Entry(entity).State = EntityState.Modified;
        }

        public async Task<SubComment> GetSubCommentAsync(DateTime time)
        {
            return await _context.SubComments.FirstOrDefaultAsync(x => x.Time == time);
        }
    }
}
