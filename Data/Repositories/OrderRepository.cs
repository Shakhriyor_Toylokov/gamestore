﻿using API.Data;
using Data.Entities;
using Data.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Data.Repositories
{
    public class OrderRepository : IOrderRepository
    {
        private readonly GameStoreDataContext _context;

        public OrderRepository(GameStoreDataContext context)
        {
            _context = context;
        }

        public async Task AddAsync(Order entity)
        {
            await _context.Orders.AddAsync(entity);
        }

        public void Delete(Order entity)
        {
            _context.Orders.Remove(entity);

        }

        public async Task<IEnumerable<Order>> GetAllAsync()
        {
            return await _context.Orders.Include(x => x.Details).AsSplitQuery().ToListAsync();
        }

        public async Task<Order> GetByIdAsync(int id)
        {
            return await _context.Orders.Include(x => x.Details).AsSplitQuery()
                                        .SingleOrDefaultAsync(x=>x.Id==id);
        }

        public void Update(Order entity)
        {
            _context.Entry(entity).State = EntityState.Modified;
        }
    }
}
