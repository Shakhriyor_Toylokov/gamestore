﻿using Business.Extensions;
using Business.Interfaces;
using Business.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [Authorize]
    public class CommentsController : BaseApiController
    {
        private readonly ICommentService _commentService;

        public CommentsController(ICommentService commentService)
        {
            _commentService = commentService;
        }
        [AllowAnonymous]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CommentModel>>> Get(int gameId)
        {
            var comments = await _commentService.GetByGameIdAsync(gameId);
            return Ok(comments);
        }

        [HttpGet("{id}", Name = "GetComment")]
        public async Task<ActionResult<CommentModel>> GetById(int id)
        {
            var comment = await _commentService.GetByIdAsync(id);
            return Ok(comment);
        }

        [HttpPost]
        public async Task<ActionResult> Add([FromBody] CommentModel model)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState.ErrorMessages());

            var comment = await _commentService.AddCommentAsync(model);
            return Created("GetComment", comment);
        }
        [AllowAnonymous]
        [HttpPost("{commentId}")]
        public async Task<ActionResult> AddSubComment([FromBody] SubCommentModel model)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState.ErrorMessages());

            var comment = await _commentService.AddSubCommentAsync(model);
            return Created("GetComment", comment);
        }
        [HttpPut]
        public async Task<ActionResult> Update([FromBody] CommentModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState.ErrorMessages());
            }
            await _commentService.UpdateAsync(model);
            return Ok(model);
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            await _commentService.DeleteAsync(id);
            return Ok();
        }
    }
}
