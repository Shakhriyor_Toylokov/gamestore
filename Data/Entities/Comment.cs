﻿namespace Data.Entities
{
    public class Comment : BaseEntity
    {
        public string Text { get; set; }
        public DateTime Time { get; set; } = DateTime.Now;
        public Game Game { get; set; }
        public int GameId { get; set; }
        public int UserId { get; set; }
        public ICollection<SubComment> SubComments { get; set; }

    }
}
