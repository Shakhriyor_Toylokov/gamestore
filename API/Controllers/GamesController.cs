﻿using Microsoft.AspNetCore.Mvc;
using Business.Interfaces;
using Business.Models;
using Business.Helpers;
using Business.Extensions;
using Business.Validation;
using Microsoft.AspNetCore.Authorization;

namespace API.Controllers
{
    [Authorize]
    public class GamesController : BaseApiController
    {
        private readonly IGameService _gameService;

        public GamesController(IGameService gameService)
        {
            _gameService = gameService;
        }
        [AllowAnonymous]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<GameModel>>> Get([FromQuery] GameParams gameParams)
        {
            var games = (gameParams.GenreId == 0) ?
                    await _gameService.GetAllAsync() :
                    await _gameService.GetByParamsAsync(gameParams);
            return Ok(games);
        }
        [AllowAnonymous]
        [HttpGet("{gamename}", Name = "GetGame")]
        public async Task<ActionResult<GameModel>> GetById(int id, string gamename)
        {
            var game = (id == 0) ?
                            await _gameService.GetByNameAsync(gamename) :
                            await _gameService.GetByIdAsync(id);
            return Ok(game);
        }

        [HttpPost]
        public async Task<ActionResult> Add([FromBody] GameModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState.ErrorMessages());
            }
            await _gameService.AddAsync(model);
            return Created("GetGame", await _gameService.GetByNameAsync(model.Name));
        }
        [HttpPost("{id}/add-photo")]
        public async Task<ActionResult<PhotoModel>> AddPhoto(int id, IFormFile file)
        {
            var photo = await _gameService.AddPhotoAsync(file, id);
            var game = await _gameService.GetByIdAsync(id);
            return CreatedAtRoute("GetGame", new { gamename = game.Name }, photo);
        }
        [HttpPut("{id}/set-main-photo/{photoid}")]
        public async Task<ActionResult> SetMainPhoto(int id, int photoId)
        {
            await _gameService.SetMainPhotoAsync(id, photoId);
            return Ok();
        }
        [HttpDelete("{id}/delete-photo/{photoId}")]
        public async Task<ActionResult> DeletePhoto(int id, int photoId)
        {
            await _gameService.DeletePhotoAsync(id, photoId);
            return Ok();
        }
        [HttpPut]
        public async Task<ActionResult> Update([FromBody] GameModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState.ErrorMessages());
            }
            await _gameService.UpdateAsync(model);
            return Ok(model);
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            await _gameService.DeleteAsync(id);
            return Ok();
        }
    }
}
