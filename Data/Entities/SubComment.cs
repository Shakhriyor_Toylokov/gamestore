﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Entities
{
    public class SubComment 
    {
        public int Id { get; set; } 
        public string Text { get; set; }
        public DateTime Time { get; set; } = DateTime.Now;
        public int UserId { get; set; }
        public Comment Comment { get; set; }
        public int CommentId { get; set; }

    }
}
