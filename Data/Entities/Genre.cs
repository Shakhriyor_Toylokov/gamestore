﻿using System.ComponentModel.DataAnnotations;

namespace Data.Entities
{
    public class Genre : BaseEntity
    {
        public string Name { get; set; }
        public ICollection<GameGenre> Games { get; set; }
    }
}